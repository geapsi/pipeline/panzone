process INDEX_SAMTOOLS {
	publishDir "${params.output_dir}/Data" , mode: 'copy'
	conda "bioconda::samtools openssl=1.0"

	input:
	path(all_file)

	output:
	path(all_file)
	path("*.fai"), emit: samtools_index

	shell:
	'''
	samtools faidx !{all_file}
	'''
}

