process ALIGNEMENT_MINIMAP2 {
	publishDir "${params.output_dir}/Data" , mode: 'copy'
	conda "bioconda::minimap2"

	input:
	tuple val(ID), path(index), path(marker_file)

	output:
	tuple val(ID), file("${ID}.paf"), emit: paf

	shell:
	"""
	minimap2 -t "${params.thread}" -paf !{index} !{marker_file} > !{ID}.paf
	"""
}

