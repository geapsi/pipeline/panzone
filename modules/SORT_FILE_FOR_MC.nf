process SORT_FILE_FOR_MC {
    publishDir "${params.output_dir}/Data", mode: 'copy'

    input:
    path file_for_mc
    tuple val(ID), file(genome)

    output: 
    path "sorted_lunch_mc" , emit: input_file_mc

    script:
    """
    awk 'NR==FNR {{ order[\$1]=++i; next }} {{ print order[\$1], \$0 }}' ${genome} ${file_for_mc} | sort -n | cut -d' ' -f2- > sorted_lunch_mc
    """
}
