process CHECK_MARKERS {

	input:
	path(markers)
	
	output:
	stdout

	shell:
	'''
	markers_file=!{markers}
	num_markers="$(grep -c ">" $markers_file)"
	if [ $num_markers -ne 2 ]
	then
        echo "
	=========================================
	The markers file must contain two markers
	========================================="
	exit 1
	fi
	'''

}


