process PGGB {
    publishDir "${params.output_dir}", mode: 'copy' 
    container 'ghcr.io/pangenome/pggb:20240410135203de71ee'

    input:
    path all_file
    path samtools_index

    output:
    path "Results/*"
    path("Results/*.smooth.final.og"), emit: og_file


    script:
    """
    file_with_all_seq=${all_file}
    number_of_haplotypes=\$(grep ">" \$file_with_all_seq | wc -l)
    pggb -i \$file_with_all_seq -n \$number_of_haplotypes -t "${params.thread}" -o Results -V "${params.ref}"    
    """
}
