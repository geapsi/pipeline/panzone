process PATHS {
    publishDir "${params.output_dir}/Analysis", mode: 'copy'
    container 'quay.io/biocontainers/odgi:0.8.6--py310h6cc9453_0'

    input:
    path og_file

    output:
    path "Paths.tsv", emit: Path_file

    script:
    """
    odgi_file=${og_file}

    odgi paths -i \$odgi_file -L > Paths.tsv
    """
}


