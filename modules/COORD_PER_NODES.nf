process COORD_PER_NODES {
    container 'nutui/my_odgi_pandas:latest'
    publishDir "${params.output_dir}/Analysis", mode: 'copy'

    input:
    path og_file
    path path_file

    output:
    path("coordinates_*.tsv"), emit: coord_node_file

    script:
    """
    python3 ${baseDir}/bin/coord_of_genotypes_per_nodes.py ${og_file} ${path_file}
    """
}
