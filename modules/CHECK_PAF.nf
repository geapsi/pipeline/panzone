process CHECK_PAF {
    input:
    tuple val(ID), path(coord_file)

    script:
    """
    unique_counts=\$(awk '{print \$6}' ${coord_file} | sort | uniq | wc -l)
    if [ "\$unique_counts" -ne "1" ]
    then
    echo "The markers align at different locations of the genome for ${ID}"
    exit 1
    fi
   
    marker_count=\$(awk '{print \$1}' ${coord_file} | sort | uniq | wc -l)
    if [ "\$marker_count" -ne "2" ]
    then
    echo "For ${ID}, the two markers do not align as expected"
    exit 1 
    fi
    """
}
