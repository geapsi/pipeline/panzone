process PERCENTAGE_PER_NODES_VIZ {
    container 'docker.io/nutui/node_count:updated'
    publishDir "${params.output_dir}/Analysis", mode: 'copy'

    input:
    path genotype_counts_file
    path coordinates_files
    path percent_files_collect

    output:
    path "proportion_of_nodes_shared_among_genotypes_with_tables.pdf"

    script:
    """
    Rscript '$baseDir/bin/proportion_node.R' ${genotype_counts_file} ${coordinates_files.join(' ')} ${percent_files_collect.join(' ')}
    """
}
