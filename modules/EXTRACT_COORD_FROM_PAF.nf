process EXTRACT_COORD_FROM_PAF {
	publishDir "${params.output_dir}/Data" , mode: 'copy'

	input:
	tuple val(ID), path(paf), path(marker_file)

	output:
	tuple val(ID), file("${ID}.zone_coord"), emit: coord

	shell:
	'''
	markel_file=!{marker_file}
	markers="$(grep ">" $markel_file |sed 's/>//g')"
	for marker in $markers
	do
	grep $marker !{paf} |sort -nrk10|head -1 >> !{ID}.zone_coord
	done
	'''
}

