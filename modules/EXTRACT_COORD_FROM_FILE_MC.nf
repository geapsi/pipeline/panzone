process EXTRACT_COORD_FROM_FILE_MC {
    publishDir "${params.output_dir}/Data", mode: 'copy'
    conda "bioconda::samtools openssl=1.0 bioconda::seqkit"

    input:
    tuple val(ID), path(coord), path(genome)

    output:
    tuple val(ID), path("${ID}.zone.fa"), emit: zone

    shell:
    '''
    zone_file=!{coord}
    extract_coord=$(awk '{if(NR==1){min=max=$8}else{min=($8<min)?$8:min;max=($9>max)?$9:max}}END{printf "%s:%s-%s\\n", $6, min, max}' "$zone_file")
    file_sense=$(awk 'NR==1 {print $5}' "$zone_file")

    if [ "$file_sense" != "+" ]
    then
    samtools faidx !{genome} $extract_coord | seqkit seq -r -p | sed 's/^>.*$/>!{ID}#1#1/' > !{ID}.zone.fa
    else
    samtools faidx !{genome} $extract_coord | sed 's/^>.*$/>!{ID}#1#1/' > !{ID}.zone.fa
    fi
    '''
}

