process GENO_PER_NODES {
    container 'nutui/my_odgi_pandas:latest'
    publishDir "${params.output_dir}/Analysis", mode: 'copy'

    input:
    path og_file
    path path_file

    output:    
    path "number_of_genotypes_per_nodes.tsv", emit: number_node_file

    script:
    """
    python3 ${baseDir}/bin/number_of_genotypes_per_nodes.py ${og_file} ${path_file}
    """
}




