process HEATMAP {
    container 'docker.io/nutui/heatmap-image'
    publishDir "${params.output_dir}/Analysis", mode: 'copy'

    input:
    path Distance_file
    path coord_node_file
    path number_node_file

    output:
    path "Heatmap_Conserved_Genomic_Regions.pdf"
    path "Heatmap_Conserved_Genomic_Regions_Without_Sliding_Windows.pdf"    
    script:
    """
    Rscript '$baseDir/bin/Heatmap.R' ${Distance_file} "${coord_node_file.join(',')}" ${number_node_file}
    Rscript '$baseDir/bin/Heatmap_no_windows.R' ${Distance_file} "${coord_node_file.join(',')}" ${number_node_file}
    """
}


