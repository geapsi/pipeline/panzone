process PERCENTAGE_PER_NODES {
    container 'nutui/my_pandas:latest'
    publishDir "${params.output_dir}/Analysis", mode: 'copy'

    input:
    path number_node_file
    each coord_node_file

    output:
    path ("*_count_percent_node.tsv"), emit: count_percent_node_file

    script:
    """
    python3 ${baseDir}/bin/count_percentages.py ${number_node_file} ${coord_node_file}
    """
}
