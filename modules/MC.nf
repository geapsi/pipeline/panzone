process MC {
    publishDir "${params.output_dir}", mode: 'copy'
    container 'quay.io/comparative-genomics-toolkit/cactus:v2.6.10'

    input:
    path input_file_mc

    output:
    path "Results/*"
    path("Results/*.og"), emit: og_file

    script:
    """
    file_with_all_seq=${input_file_mc}
    cactus-pangenome tmp/ \$file_with_all_seq --outDir Results/ --outName pangenome_mc_odgi_stats --odgi --stats --reference "${params.ref}"
    """
}
