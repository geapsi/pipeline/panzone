process JACCARD {
    container 'docker.io/nutui/jaccard_tree_image'
    publishDir "${params.output_dir}/Analysis", mode: 'copy'

    input:
    path Distance_file

    output:
    path "Jaccard_Phylogenetic_Tree.pdf"
    
    script:
    """
    Rscript '$baseDir/bin/Jaccard_Phylogenetic_Tree.R' ${Distance_file}
    """
}


