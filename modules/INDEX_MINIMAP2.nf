process INDEX_MINIMAP2 {
    conda "bioconda::minimap2"

    input:
    tuple val(ID), path(genome)

    output:
    tuple val(ID), file("${ID}.mmi"), emit: index


    script:
    """
    minimap2 -d "${ID}.mmi" "${genome}"
    """
}
