process CHECK_GENOMES {

        input:
	path(genomes)

        output:
        stdout

        shell:
	'''
	genomes_file=!{genomes}
	
	column_names="$(head -n 1 $genomes_file)"
	if [ "$column_names" != "ID	genome" ]
	then
        echo "
	============================================================================
	Error: The column names in the genomeChannel file must be 'ID' and 'genome'.
	============================================================================"
        exit 1
	fi

	num_files="$(tail -n +2 $genomes_file | wc -l)"
	if [ $num_files -lt 2 ]
	then
        echo "
	=========================================================
	Error: The genome file must contain at least two entries.
	=========================================================
	"
        exit 1
	fi

	duplicated_ids="$(cut -f 1 $genomes_file | sort | uniq -d)"
	if [ -n "$duplicated_ids" ]
	then
        echo "
	=========================================================
	Error: The following IDs are not unique: $duplicated_ids
	========================================================"
        exit 1
	fi

	while IFS=$'\t' read -r id genome
	do
	if [ -n "$id" ] && [ -n "$genome" ]
	then
	if [ -z "$id" ] || [ -z "$genome" ]
	then
	echo "
	===========================================================
	Error: Both IDs and genomes must be provided for each line.
	==========================================================="
	exit 1
        fi
	fi
	done < $genomes_file   

	'''

}

