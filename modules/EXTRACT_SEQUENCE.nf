process EXTRACT_SEQUENCE {
    publishDir "${params.output_dir}/Data", mode: 'copy'
    conda "bioconda::samtools openssl=1.0"

    input:
    tuple val(ID), file(genome), val(coord)

    output: 
    path("${ID}.zone.fa"), emit: zone

    shell:
    '''
    samtools faidx !{genome} !{coord} > !{ID}.zone.fa
    sed -i "s/^>.*$/>!{ID}#1#1/" !{ID}.zone.fa
    '''
}

