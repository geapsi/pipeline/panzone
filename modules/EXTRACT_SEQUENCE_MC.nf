process EXTRACT_SEQUENCE_MC {
    publishDir "${params.output_dir}/Data", mode: 'copy'
    conda "bioconda::samtools openssl=1.0"

    input:
    tuple val(ID), file(genome), val(coord)

    output:
    tuple val(ID), path("${ID}.zone.fa"), emit: zone

    shell:
    '''
    samtools faidx !{genome} !{coord} > !{ID}.zone.fa
    sed -i "s/^>.*$/>!{ID}#1#1/" !{ID}.zone.fa
    '''
}
