include { CHECK_MARKERS } from '../modules/CHECK_MARKERS.nf'
include { CHECK_GENOMES } from '../modules/CHECK_GENOMES.nf'
include { INDEX_MINIMAP2 } from '../modules/INDEX_MINIMAP2.nf'
include { ALIGNEMENT_MINIMAP2 } from '../modules/ALIGNEMENT_MINIMAP2.nf'
include { EXTRACT_COORD_FROM_PAF } from '../modules/EXTRACT_COORD_FROM_PAF.nf'
include { CHECK_PAF } from '../modules/CHECK_PAF.nf'
include { EXTRACT_COORD_FROM_FILE } from '../modules/EXTRACT_COORD_FROM_FILE.nf'
include { EXTRACT_COORD_FROM_FILE_MC } from '../modules/EXTRACT_COORD_FROM_FILE_MC.nf'
include { SORT_FILE_FOR_MC } from '../modules/SORT_FILE_FOR_MC.nf'
include { MC } from '../modules/MC.nf'
include { INDEX_SAMTOOLS } from '../modules/INDEX_SAMTOOLS.nf'
include { PGGB } from '../modules/PGGB.nf'
include { DISTANCE } from '../modules/DISTANCE.nf'
include { JACCARD } from '../modules/JACCARD.nf'
include { PATHS } from '../modules/PATHS.nf'
include { GENO_PER_NODES } from '../modules/GENO_PER_NODES.nf'
include { COORD_PER_NODES } from '../modules/COORD_PER_NODES.nf'
include { PERCENTAGE_PER_NODES } from '../modules/PERCENTAGE_PER_NODES.nf'
include { PERCENTAGE_PER_NODES_VIZ } from '../modules/PERCENTAGE_PER_NODES_VIZ.nf'
include { HEATMAP } from '../modules/HEATMAP.nf'

workflow WORKFLOW_MARKERS {

    take:
    markersChannel
    genomeChannel
    genome_FileChannel
    indexChannel

    main:

switch (params.tool) {

    case 'PGGB':
    CHECK_MARKERS(markersChannel)
    CHECK_GENOMES(genome_FileChannel)
    
    def allIndexesChannel
    if (indexChannel) {
    allIndexesChannel = indexChannel 
    } else {
    allIndexesChannel = INDEX_MINIMAP2(genomeChannel) 
    }
    index_marker = allIndexesChannel.combine(markersChannel)

    ALIGNEMENT_MINIMAP2(index_marker)
    paf_marker = ALIGNEMENT_MINIMAP2.out.paf.combine(markersChannel)
    EXTRACT_COORD_FROM_PAF(paf_marker)
    paf_file = EXTRACT_COORD_FROM_PAF.out.coord
    CHECK_PAF(paf_file)
    coord_genome = EXTRACT_COORD_FROM_PAF.out.coord.join(genomeChannel)
    EXTRACT_COORD_FROM_FILE(coord_genome) | collectFile(name: "${params.output_dir}/Data/all_seq.fa") | set { all_file }
    INDEX_SAMTOOLS(all_file)
    PGGB(all_file, INDEX_SAMTOOLS.out.samtools_index.collect())
    DISTANCE(PGGB.out.og_file)
    JACCARD(DISTANCE.out.Distance_file)
    PATHS(PGGB.out.og_file)
    GENO_PER_NODES(PGGB.out.og_file, PATHS.out.Path_file)
    coord_node_files = COORD_PER_NODES(PGGB.out.og_file, PATHS.out.Path_file).collect()
    PERCENTAGE_PER_NODES(GENO_PER_NODES.out.number_node_file, coord_node_files)
    percent_files_collect = PERCENTAGE_PER_NODES.out.count_percent_node_file.collect()
    PERCENTAGE_PER_NODES_VIZ(GENO_PER_NODES.out.number_node_file, coord_node_files ,percent_files_collect) 
    HEATMAP(DISTANCE.out.Distance_file, COORD_PER_NODES.out.coord_node_file, GENO_PER_NODES.out.number_node_file)


    break


    case 'MC':
    CHECK_MARKERS(markersChannel)
    CHECK_GENOMES(genome_FileChannel)


    def allIndexesChannel
    if (indexChannel) {
    allIndexesChannel = indexChannel
    } else {
    allIndexesChannel = INDEX_MINIMAP2(genomeChannel)
    }
    index_marker = allIndexesChannel.combine(markersChannel)

    ALIGNEMENT_MINIMAP2(index_marker)
    paf_marker = ALIGNEMENT_MINIMAP2.out.paf.combine(markersChannel)
    EXTRACT_COORD_FROM_PAF(paf_marker)
    paf_file = EXTRACT_COORD_FROM_PAF.out.coord
    CHECK_PAF(paf_file)
    coord_genome = EXTRACT_COORD_FROM_PAF.out.coord.join(genomeChannel)
    EXTRACT_COORD_FROM_FILE_MC(coord_genome)
                .map { id, file -> "${id}\t${file}\n" }
                .collectFile(name: "${params.output_dir}/Data/lunch_mc") | set { file_for_mc }
            genomeChannel
                .map { id, genome -> tuple(id, genome) }
                .set { genome_channel_with_files }

    SORT_FILE_FOR_MC(file_for_mc, genome_channel_with_files)
    MC(SORT_FILE_FOR_MC.out.input_file_mc)
    DISTANCE(MC.out.og_file)
    JACCARD(DISTANCE.out.Distance_file)
    PATHS(MC.out.og_file)
    GENO_PER_NODES(MC.out.og_file, PATHS.out.Path_file)
    coord_node_files = COORD_PER_NODES(MC.out.og_file, PATHS.out.Path_file).collect()
    PERCENTAGE_PER_NODES(GENO_PER_NODES.out.number_node_file, coord_node_files)
    percent_files_collect = PERCENTAGE_PER_NODES.out.count_percent_node_file.collect()
    PERCENTAGE_PER_NODES_VIZ(GENO_PER_NODES.out.number_node_file, coord_node_files ,percent_files_collect)
    HEATMAP(DISTANCE.out.Distance_file, COORD_PER_NODES.out.coord_node_file, GENO_PER_NODES.out.number_node_file)

    break
}
}


