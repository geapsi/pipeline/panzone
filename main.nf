params.thread = 4
params.coords_file = null
params.markers_file = null
params.output_dir = "Panzone_Output"
params.tool = "PGGB"
params.index_file = null


def genomeFile = file(params.genome_file)
def firstId = genomeFile.readLines().drop(1).collect { it.split('\t')[0] }.find()

if (!params.ref) {
    params.ref = firstId
}

def validTools = ['PGGB', 'MC']
if (!validTools.contains(params.tool)) {
    throw new IllegalArgumentException("Invalid tool specified. Valid options are: ${validTools.join(', ')}")
}


if (!params.ref) {
    params.ref = firstId
}

log.info """\
         P A N Z O N E -  P I P E L I N E
         ===================================
         Genome_file: ${params.genome_file}
         Markers_file: ${params.markers_file ?: 'Not specified'}
         Coords_file: ${params.coords_file ?: 'Not specified'}
         Index_file: ${params.index_file ?: 'Not specified'}
         Output_Dir: ${params.output_dir ?: 'Panzone_Output'}
         Reference: ${params.ref}
         Threads: ${params.thread}
         Tool : ${params.tool}
         ===================================
         """

include {WORKFLOW_MARKERS} from './subworkflows/workflow_markers.nf'
include {WORKFLOW_COORD} from './subworkflows/workflow_coord.nf'

workflow {

    if (!params.genome_file) {
        log.error("No genome file specified. Please specify the genome file with '--genome_file'.")
        System.exit(1)
    }

    genomeChannel = Channel.fromPath(params.genome_file).splitCsv(header: true, sep:"\t").map{ row -> tuple(row.ID, file(row.genome)) }

    indexChannel = params.index_file ? 
        Channel.fromPath(params.index_file).splitCsv(header: true, sep: "\t").map { row -> tuple(row.ID, file(row.index)) } : 
        null


    if (params.coords_file && !params.markers_file) {
        WORKFLOW_COORD(params.coords_file, genomeChannel)

    } else if (params.markers_file && !params.coords_file) {
        markersChannel = Channel.fromPath(params.markers_file)
        genome_fileChannel = Channel.fromPath(params.genome_file)
        WORKFLOW_MARKERS(markersChannel, genomeChannel, genome_fileChannel, indexChannel)


    } else if (params.coords_file && params.markers_file) {
        log.error("Both coords_file and markers_file specified. Please specify only one.")
        System.exit(1)

    } else {
        log.error("No input file specified. Please specify either 'coords_file' or 'markers_file'.")
        System.exit(1)
    }
}
