#!/usr/bin/env python3
import sys
import pandas as pd
import odgi

def load_paths(file_path):
    with open(file_path, 'r') as file:
        paths = [line.strip() for line in file]
    return paths

def increment_node_count(graph, handle, node_genotype_count, current_path):
    node_id = graph.get_id(handle)
    if node_id not in node_genotype_count:
        node_genotype_count[node_id] = set()
    node_genotype_count[node_id].add(current_path)
    return True

graph_file = sys.argv[1]
paths_file = sys.argv[2]

graph = odgi.graph()
graph.load(graph_file)

paths_of_interest = load_paths(paths_file)

node_genotype_count = {}

for path_name in paths_of_interest:
    current_path = path_name
    path_handle = graph.get_path_handle(path_name)
    graph.for_each_step_in_path(path_handle, lambda step_handle: increment_node_count(graph, graph.get_handle_of_step(step_handle), node_genotype_count, current_path))

genotype_counts = {node_id: len(genotypes) for node_id, genotypes in node_genotype_count.items()}
df = pd.DataFrame(list(genotype_counts.items()), columns=["Node ID", "Genotype Count"])

output_file = "number_of_genotypes_per_nodes.tsv"
df.to_csv(output_file, sep='\t', index=False)


