import sys
import odgi
import pandas as pd

def load_graph(og_file):
    graph = odgi.graph()
    graph.load(og_file)
    return graph

def load_paths(paths_file):
    with open(paths_file, 'r') as f:
        paths_of_interest = [line.strip() for line in f]
    return paths_of_interest

def calculate_coordinates_for_path(graph, path_name):
    path_handle = graph.get_path_handle(path_name)
    results = []
    current_start = 1
    current_end = 0

    def calculate_coordinates(step_handle):
        nonlocal current_start, current_end
        handle = graph.get_handle_of_step(step_handle)
        node_id = graph.get_id(handle)
        node_length = graph.get_length(handle)

        current_end = current_start + node_length - 1

        results.append({
            "node_id": node_id,
            "coord_genotype": f"{current_start}-{current_end}"
        })

        current_start = current_end + 1
        return True

    graph.for_each_step_in_path(path_handle, calculate_coordinates)
    df = pd.DataFrame(results)
    output_file = f"coordinates_{path_name.replace('#', '_')}.tsv"
    df.to_csv(output_file, sep='\t', index=False)

def main(og_file, paths_file):
    graph = load_graph(og_file)
    paths_of_interest = load_paths(paths_file)
    for path_name in paths_of_interest:
        calculate_coordinates_for_path(graph, path_name)

if __name__ == "__main__":
    og_file = sys.argv[1]
    paths_file = sys.argv[2]
    main(og_file, paths_file)

