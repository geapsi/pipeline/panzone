#!/usr/bin/env Rscript

library(ggplot2)
library(tidyr)
library(dplyr)
library(tools)
library(gridExtra)
library(readr)
library(RColorBrewer)
library(grid)

args <- commandArgs(trailingOnly = TRUE)
if (length(args) < 3) stop("Usage: Rscript script.R number_of_genotypes_per_nodes.tsv coordinates_files... percentage_files...")

genotype_counts_file <- args[1]
coordinate_files_list <- list()
percentage_files_list <- list()

for (file in args[-1]) {
  base_name <- basename(file)
  if (grepl("^coordinates_(.+?)(_[0-9_]+)?\\.tsv$", base_name)) {
    genotype_id <- sub("^coordinates_(.+?)(_[0-9_]+)?\\.tsv$", "\\1", base_name)
    coordinate_files_list[[genotype_id]] <- file
  } else if (grepl("^(.*)_count_percent_node\\.tsv$", base_name)) {
    genotype_id <- sub("^(.*)_count_percent_node\\.tsv$", "\\1", base_name)
    percentage_files_list[[genotype_id]] <- file
  } else {
    stop(paste("Fichier inconnu :", file))
  }
}

common_genotype_ids <- intersect(names(coordinate_files_list), names(percentage_files_list))
if (length(common_genotype_ids) == 0) stop("Aucun génotype commun trouvé.")

genotype_counts <- read_tsv(genotype_counts_file, col_types = cols())

prepare_data <- function(file) {
  data <- read.table(file, header = TRUE, sep = "\t")
  p <- data[seq(1, nrow(data), by = 2), ]
  c <- data[seq(2, nrow(data), by = 2), ]
  c$Genotype <- p$Genotype
  lp <- gather(p, "Node", "Percentage", -Genotype)
  lc <- gather(c, "Node", "Count", -Genotype)
  lp$Node <- sub("^X", "", lp$Node)
  lc$Node <- sub("^X", "", lc$Node)
  long_data <- left_join(lp, lc, by = c("Genotype", "Node")) %>%
    mutate(Legend_Label = paste(Node, ": ", sprintf("%.1f%%", Percentage), " (", Count, " nodes)")) %>%
    mutate(Node_num = as.numeric(Node)) %>%
    arrange(Node_num) %>%
    select(-Node_num)
  long_data$Legend_Label <- factor(long_data$Legend_Label, levels = unique(long_data$Legend_Label))
  long_data
}

create_palette <- function(n) {
  base_palette <- brewer.pal(12, "Paired")
  if (n <= 12) base_palette[1:n] else colorRampPalette(base_palette)(n)
}

create_donut_chart <- function(df, genotype) {
  ggplot(df, aes(x = 2, y = Percentage, fill = Legend_Label)) +
    geom_bar(stat = "identity") +
    coord_polar(theta = "y", start = 0) +
    xlim(0.5, 2.5) +
    theme_void() +
    theme(legend.position = "right", plot.title = element_text(size = 10),
          legend.text = element_text(size = 8), legend.title = element_text(size = 8)) +
    labs(title = paste("Donut Chart of Percentages of Nodes Shared Among Genotypes for", genotype),
         fill = "Node") +
    scale_fill_manual(values = create_palette(length(unique(df$Legend_Label))))
}

prepare_table_data <- function(genotype_counts, coordinates_file) {
  coordinates <- read_tsv(coordinates_file, col_types = cols()) %>%
    rename(Node_ID = node_id) %>%
    separate(coord_genotype, c("Start", "End"), sep = "-", convert = TRUE) %>%
    mutate(Number_of_Bases = End - Start + 1) %>%
    select(Node_ID, Number_of_Bases)
  merged_data <- genotype_counts %>%
    rename(Node_ID = `Node ID`) %>%
    inner_join(coordinates, by = "Node_ID")
  merged_data %>%
    group_by(`Number of Genotypes Crossed` = `Genotype Count`) %>%
    summarise(`Total Number of Nodes` = n(), `Total Number of Bases` = sum(Number_of_Bases)) %>%
    arrange(`Number of Genotypes Crossed`)
}

pdf("proportion_of_nodes_shared_among_genotypes_with_tables.pdf", width = 8, height = 10)

for (genotype_id in common_genotype_ids) {
  data <- prepare_data(percentage_files_list[[genotype_id]])
  table_data <- prepare_table_data(genotype_counts, coordinate_files_list[[genotype_id]])
  tg <- tableGrob(table_data, rows = NULL)
  for (g in unique(data$Genotype)) {
    d <- subset(data, Genotype == g)
    print(create_donut_chart(d, g))
    grid.newpage()
    grid.draw(tg)
  }
}

dev.off()