import pandas as pd
import sys
import os

genotypes_file = sys.argv[1]
coordinates_file = sys.argv[2]

genotype = os.path.basename(coordinates_file).split('_')[1]

coordinates_df = pd.read_csv(coordinates_file, sep='\t', names=['node_id', 'coord_genotype'], dtype=str, low_memory=False)
genotypes_df = pd.read_csv(genotypes_file, sep='\t', names=['node_id', 'genotype_count'], dtype={'node_id': str, 'genotype_count': str}, low_memory=False)

genotypes_df = genotypes_df[pd.to_numeric(genotypes_df['genotype_count'], errors='coerce').notnull()]
genotypes_df['genotype_count'] = genotypes_df['genotype_count'].astype(int)

merged_df = pd.merge(coordinates_df, genotypes_df, on='node_id')
merged_df.columns = ['node_id', 'coord_genotype', 'Genotype_Count']

count_genotypes = merged_df['Genotype_Count'].value_counts().sort_index()
percent_genotypes = (count_genotypes / count_genotypes.sum()) * 100

max_genotypes = genotypes_df['genotype_count'].max()

result_df = pd.DataFrame(columns=['Genotype'] + [str(i) for i in range(1, max_genotypes + 1)])
result_df.loc[0] = [genotype] + [percent_genotypes.get(i, 0) for i in range(1, max_genotypes + 1)]
result_df.loc[1] = ['Count'] + [count_genotypes.get(i, 0) for i in range(1, max_genotypes + 1)]

output_filename = f'{genotype}_count_percent_node.tsv'
result_df.to_csv(output_filename, sep='\t', index=False)
