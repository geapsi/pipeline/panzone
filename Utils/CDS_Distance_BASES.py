import sys
import pandas as pd
from collections import defaultdict
import argparse


def prepare_bases_dict(row):
    nodes = str(row['PATH_NODE']).split(">")
    bases = list(map(int, str(row['BASES_PER_NODE']).split(">")))
    return dict(zip(nodes, bases))


def get_bases_grouped_by_gene(all_genes):
    gene_bases = defaultdict(lambda: defaultdict(int))

    for index, row in all_genes.iterrows():
        gene_info = row["GENE_INFO"].split("_")[-1]
        bases_dict = row["BASES_DICT"]

        for node, bases in bases_dict.items():
            gene_bases[gene_info][node] += bases

    return gene_bases


def compute_jaccard_distance_on_bases(files):
    all_genes = pd.DataFrame()
    for file in files:
        genotype_name = file.replace('_node_bases_CDS.tsv', '')
        df = pd.read_csv(file, sep='\t')
        df['Genotype'] = genotype_name
        df['BASES_DICT'] = df.apply(prepare_bases_dict, axis=1)
        all_genes = pd.concat([all_genes, df], ignore_index=True)

    gene_bases = get_bases_grouped_by_gene(all_genes)

    long_dist = pd.DataFrame()
    for gene1, bases_dict1 in gene_bases.items():
        for gene2, bases_dict2 in gene_bases.items():
            common_nodes = set(bases_dict1.keys()).intersection(bases_dict2.keys())
            sum_bases_intersection = sum(min(bases_dict1[node], bases_dict2[node]) for node in common_nodes)

            all_nodes = set(bases_dict1.keys()).union(bases_dict2.keys())
            sum_bases_union = sum(
                max(bases_dict1.get(node, 0), bases_dict2.get(node, 0))
                for node in all_nodes
            )

            jaccard_distance = sum_bases_intersection / sum_bases_union if sum_bases_union > 0 else 0

            temp = pd.DataFrame(
                {
                    'g1': [gene1],
                    'g2': [gene2],
                    'dist': [jaccard_distance]
                }
            )
            long_dist = pd.concat([long_dist, temp], ignore_index=True)

    return long_dist


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Compute Jaccard distance between genes based on bases from multiple CDS TSV files.')
    parser.add_argument('files', metavar='F', type=str, nargs='+', help='TSV files to process')
    parser.add_argument('--output', type=str, default='Jaccard_distance_bases_CDS.tsv', help='Output file name (default: Jaccard_distance_bases_CDS.tsv)')

    args = parser.parse_args()

    try:
        long_dist = compute_jaccard_distance_on_bases(args.files)
        long_dist.to_csv(args.output, sep="\t", index=False)
        print(f"[INFO] Distances written to {args.output}")
    except KeyError as e:
        print(f"[ERROR] Missing column in input files: {e}")
        sys.exit(1)