# Clustering of Genes Based on Pangenome Information

The following two scripts allow the creation of a **heatmap** for clustering genes based on **pangenome information**, closely related to **intra-accession synteny information**.

## Step 1: Extracting CDS Coordinates

First, the **Python script `CDS_Extract_coord_BASES.py`** is executed as follows:

```bash  
python3 CDS_Extract_coord_BASES.py <PATH> <GFF> <OG file> CDS

```

For example, using this command:


```bash  

python3 CDS_Extract_coord_BASES.py CAMEOR#1#1 ../../CAMEOR#1#1_helixer_gene_of_interest.gff ../../all_seq.fa.bf3285f.11fba48.f711e2a.smooth.final.og CDS
```

This will generate the file:
```
CAMEOR_node_bases_CDS.tsv

```

## Step 2: Calculating Jaccard Distances

After executing this command for all accessions, we can run the Python script CDS_Distance_BASES.py on all generated files using:

```bash
docker run --rm -v $(pwd):/workspace -w /workspace nutui/my_odgi_pandas_with_gffutils:latest python3 CDS_Distance_BASES.py CAMEOR_node_bases_CDS.tsv <PATH>_node_bases_CDS.tsv <PATH>_node_bases_CDS.tsv <PATH>_node_bases_CDS.tsv
```

This command produces the output file:
```
Jaccard_distance_bases_CDS.tsv
```

## Step 3: Heatmap Visualization
Finally, we can visualize the clustering by executing the R script Pheatmap.R as follows:

```bash
docker run --rm -v $(pwd):/workspace -v $(pwd)/..:/parent_dir -w /workspace nutui/heatmap-image-with-pheatmap:latest Rscript /parent_dir/Pheatmap.R Jaccard_distance_bases_CDS.tsv Jaccard_distance_bases_CDS BASE_CDS_MYB_PGGB
```
