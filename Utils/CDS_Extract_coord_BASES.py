import sys
import argparse
import pandas as pd
#sys.path.append('/home/bherlemont/odgi/lib/')
import odgi
import gffutils


def read_positions_from_gff(filepath, feature_type):
    db = gffutils.create_db(filepath, dbfn=':memory:', force=True, keep_order=True, merge_strategy='merge', sort_attribute_values=True)
    gene_positions = {}
    for gene in db.features_of_type('gene'):
        gene_id = gene.id
        gene_positions[gene_id] = {
            "gene": (gene.start, gene.end),
            "cds": []
        }
        if feature_type == "CDS":
            for cds in db.children(gene, featuretype='CDS', order_by='start'):
                cds_id = cds.id
                gene_positions[gene_id]["cds"].append((cds.start, cds.end, cds_id))
    print(f"[INFO] Loaded {len(gene_positions)} genes from GFF.")
    return gene_positions


def calculate_base_count(node_data):
    total_bases = 0
    bases_per_node = []
    for node in node_data:
        adjusted_start = node['adjusted_start']
        adjusted_end = node['adjusted_end']
        bases_in_node = adjusted_end - adjusted_start + 1
        total_bases += bases_in_node
        bases_per_node.append(bases_in_node)

    return total_bases, bases_per_node


def process_positions(gr, path_handle, start_pos, end_pos, feature_info_formatted):
    summary_data = []
    node_data = []  
    current_pos = 0
    step = gr.path_begin(path_handle)

    while step != gr.path_end(path_handle):
        handle = gr.get_handle_of_step(step)
        node_id = str(gr.get_id(handle))
        node_length = gr.get_length(handle)
        node_start = current_pos
        node_end = current_pos + node_length - 1

        if node_end >= start_pos and node_start <= end_pos:
            adjusted_start = max(node_start, start_pos)
            adjusted_end = min(node_end, end_pos)

            node_data.append({
                'node_id': node_id,
                'adjusted_start': adjusted_start,
                'adjusted_end': adjusted_end
            })

        current_pos += node_length
        if current_pos >= end_pos:
            break
        step = gr.get_next_step(step)

    total_bases, bases_per_node = calculate_base_count(node_data)

    summary_data.append({
        'GENE_INFO': feature_info_formatted,
        'PATH_NODE': ">".join([node['node_id'] for node in node_data]),
        'number_of_bases_in_the_path': total_bases,
        'BASES_PER_NODE': ">".join(map(str, bases_per_node)),
        'BORDER': f"{start_pos}-{end_pos}",
        'DIRECTION': '+' if start_pos < end_pos else '-'
    })

    return summary_data


def process_graph(path_name, gff_file, og_file, feature_type, output_file):
    gr = odgi.graph()
    gr.load(og_file)
    path_handle = gr.get_path_handle(path_name)

    gene_positions = read_positions_from_gff(gff_file, feature_type)

    summary_data = []

    for gene_id, positions in gene_positions.items():
        gene_start, gene_end = positions["gene"]
        gene_info_formatted = f"{gene_start}_{gene_end}_{gene_id}"
        print(f"[INFO] Processing Gene {gene_info_formatted}: Start {gene_start}, End {gene_end}")
        summary_data.extend(process_positions(gr, path_handle, gene_start, gene_end, gene_info_formatted))

        if feature_type == "CDS":
            for cds_start, cds_end, cds_id in positions["cds"]:
                cds_info_formatted = f"{cds_start}_{cds_end}_{cds_id}_{gene_id}"
                print(f"[INFO] Processing CDS {cds_info_formatted}: Start {cds_start}, End {cds_end}")
                summary_data.extend(process_positions(gr, path_handle, cds_start, cds_end, cds_info_formatted))

    summary_df = pd.DataFrame(summary_data)
    summary_df.to_csv(output_file, sep='\t', index=False)
    print(f"[INFO] Output written to {output_file}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process ODGI graph and extract node information for genes/CDS.')
    parser.add_argument('path_name', type=str, help='Path name in the graph')
    parser.add_argument('gff_file', type=str, help='Path to the GFF file')
    parser.add_argument('og_file', type=str, help='Path to the OG file')
    parser.add_argument('feature_type', type=str, nargs='?', default='CDS', help='Feature type to process (GENE or CDS)')

    args = parser.parse_args()

    path_name_parts = args.path_name.split('#')
    path_part = path_name_parts[0] if len(path_name_parts) > 0 else 'output'

    if args.feature_type == 'CDS':
        output_file = f"{path_part}_node_bases_CDS.tsv"
    else:
        output_file = f"{path_part}_node_bases_GENE.tsv"

    process_graph(args.path_name, args.gff_file, args.og_file, args.feature_type, output_file)
