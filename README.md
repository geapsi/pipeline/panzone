# PanZone

PanZone is a Nextflow-based pipeline that enables the analysis of a zone of interest present in different accessions through a pangenome created by PanGenomeGraphBuilder (PGGB) and Minigraph-Cactus (MC).

## Table of Contents

- [Introduction](#introduction)
- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Installation Verification](#installation-verification)
- [Usage](#usage)
  - [Input Files](#input-files)
  - [Run Command](#run-command)
    - [With a marker genes file](#with-a-marker-genes-file)
    - [With a coordinates file](#with-a-coordinates-file)
- [Output](#output)
  - [Data](#data)
  - [Results](#results)
  - [Analysis](#analysis)
- [Parameters](#parameters)
- [Future Improvements](#future-improvements)


## Introduction

PanZone is designed to facilitate the analysis of zones of interest from a pangenome. Using Nextflow to orchestrate the pipeline steps, PanZone integrates various tools and dependencies, including PGGB and MC, to provide a comprehensive and reproducible analysis. This pipeline is particularly useful for assessing the conservation between different loci using only the information from the pangenome.

## Prerequisites

Before installing and using PanZone, ensure that the following software is installed on your system:

- [Nextflow](https://www.nextflow.io/)
- [Conda](https://conda.io/projects/conda/en/latest/user-guide/install/index.html)
- [Mamba](https://anaconda.org/conda-forge/mamba)
- [Docker](https://docs.docker.com/engine/install/)

## Installation

To install PanZone, clone the repository from ForgeMia using the following git command:

```sh
git clone git@forgemia.inra.fr:geapsi/pipeline/panzone.git
```

## Installation Verification

After downloading the pipeline, to ensure it is functional, you can run either of the following commands:

```sh
nextflow run main.nf --genome_file example_dataset/genomes_TEST_PGGB.tsv --markers_file example_dataset/test_genes_markers.fasta
```

```sh
nextflow run main.nf --genome_file example_dataset/genomes_TEST_MC.tsv --markers_file example_dataset/test_genes_markers.fasta --tool MC
```

```sh
nextflow run main.nf --genome_file example_dataset/genomes_TEST_PGGB.tsv --coords_file example_dataset/coord_TEST_PGGB.tsv
```

```sh
nextflow run main.nf --genome_file example_dataset/genomes_TEST_MC.tsv --coords_file example_dataset/coord_TEST_MC.tsv --tool MC
```

The pipeline should run without errors.

## Usage

### Input Files

You will need to provide an input file in tabular format detailing the location of the genomes and a specific ID for each file. An example of the expected file type is available in `example_dataset/genomes_TEST_<PGGB-MC>.tsv`.

Then, depending on your case, you can provide:

- Either a fasta file containing two flanking marker genes that will be used to delineate the zone of interest in the genome. Example: `example_dataset/test_genes_markers.fasta`.
- Or a coordinates file that will be used to extract the coordinates in each genome. Example: `example_dataset/coord_TEST<PGGB-MC>.tsv`.

The files must have the same format: separator and column names as the example files.

*Note: As the order matters in MC, the order used by the tool will be the same as the one specified in the file provided with the `--genome_file` argument.*


### Run Command

To run the pipeline, use one of the following commands based on your input data:

#### With a marker genes file

If you want to use flanking markers to determine a locus of interest, create the pangenome with these markers, and analyze it, use the following command:

```sh
nextflow run main.nf --genome_file <your_genome_file.tsv> --markers_file <your_genes_markers_file.fasta> --tool <PGGB;MC>

```

**Note**: When using whole genomes, the indexing step with Minimap2 can be time-consuming, especially if you plan to run the pipeline repeatedly. To avoid this, you can provide precomputed indexes with the following command:
 
 ```sh
 nextflow run main.nf --genome_file <your_genome_file.tsv> --markers_file <your_genes_markers_file.fasta> --tool <PGGB;MC> --index_file <your_index_file.tsv>
 ```
 
An example of the `--index_file` format is available in `example_dataset/index_TEST_<PGGB-MC>.tsv`. This file should be tab-delimited with two columns: `ID`, which matches the IDs in `example_dataset/genomes_TEST_<PGGB-MC>.tsv`, and `index`, which contains the paths to the index files.
 
 Ensure that:
 - The IDs are in the same order in both the genome and index files.
 - All required indexes are present.


#### With a coordinates file

If you already have the coordinates of a locus, you can use the following command:

```sh
nextflow run main.nf --genome_file <your_genome_file.tsv> --coords_file <your_coords_file.tsv> --tool <PGGB;MC>
```

The pipeline will delineate the loci, create a pangenome with the specified regions, and then perform the analysis.

## Output

Once the pipeline execution is complete, the output directory (default is `Panzone_Output` if not specified) will contain three folders: `Data`, `Results`, and `Analysis`.

### Data
This folder contains all the files obtained during the preprocessing steps before the creation of the pangenome. The main files of interest here are the FASTA files of the loci.

### Results
This folder contains the output files from the tools PGGB or MC, including all created files. Key files in this folder include:
- Visual representations
- GFA file
- OG file
- VCF file

### Analysis
This folder contains all the analysis files. 

Visual representations include:
- `Jaccard_Phylogenetic_Tree.pdf` : A phylogenetic tree of the genotypes based on the overall Jaccard distance
- `proportion_of_nodes_shared_among_genotypes_with_tables.pdf` :A representation of the number of common/specific nodes for each genotype through a donut chart and a table showing the number of nodes and the number of common/specific bases between genotypes. All the figures for all genotypes are in a single PDF file.
- `Heatmap_Conserved_Genomic_Regions.pdf` :A heatmap representing the conservation zones between genotypes obtained by rolling sliding windows, ordered according to the Jaccard distance : This type of representation makes it easier to observe minor events.
- `Heatmap_Conserved_Genomic_Regions_Without_Sliding_Windows.pdf` :A heatmap representing the conservation zones between genotypes obtained by rolling sliding windows, ordered according to the Jaccard distance : This type of representation provides a better overall view of the events.

Key files in this folder include:
- `coordinates_GENOTYPE.tsv`: Contains all the coordinates associated with the nodes for each genotype.
- `GENOTYPE_count_percent_node.tsv`: Contains the count of shared or non-shared nodes between the genotypes for each genotype.
- `Paths.tsv`: Contains the names of the paths in the final graph (for MC, some genotypes might not appear).
- `Distance.tsv`: Contains the various distances between genotypes on the graph.




## Parameters

| Parameter | Default Value | Description | Mandatory |
| --- | --- | --- | --- |
| `--ref` | First value in `--genome_file` | Specify the reference genome. If not provided, the first genome listed in the `--genome_file` is used. | No  |
| `--thread` | 4   | Set the number of CPUs to use. | No  |
| `--output_dir` | `Panzone_Output` | Define the output directory. | No  |
| `--genome_file` | None | Specify the genome file in TSV format. | Yes |
| `--markers_file` | None | Specify the markers file in FASTA format. Required if not using `--coords_file`. | Conditional |
| `--coords_file` | None | Specify the coordinates file in TSV format. Required if not using `--markers_file`. | Conditional |
| `--tool` | PGGB | Specify the tool to use for the analysis. &lt;PGGB&gt; or &lt;MC&gt; | No  |
| `--index_file`| None | Specify the genome file in TSV format. |No |

## Future Improvements

- Add the possibility to use PGGB without smoothxg.
- Add the possibility to provide a .gfa or .og input file and perform only the analysis part.
- Add the possiiblity of naming files with a suffix.
- Modify the code to create a single figure with all te donut chart 
- Add the option to choose the number of sliding windows.
- Add the number of bases represented by the nodes in the donut chart